from django.conf.urls import patterns, url

from . import views


urlpatterns = patterns('',
    url(r'^$', views.CourseListView.as_view(), name='list'),
    url(r'^create/$', views.CourseCreateView.as_view(), name='create'),
    url(r'^(?P<slug>[\w-]+)/$',
        views.CategoryView.as_view(), name='category'),
    url(r'^(?P<category_slug>[\w-]+)/(?P<slug>[\w-]+)/join/$',
        views.CourseJoinView.as_view(), name='join'),
    url(r'^(?P<category_slug>[\w-]+)/(?P<slug>[\w-]+)/leave/$',
        views.CourseLeaveView.as_view(), name='leave'),
    url(r'^(?P<category_slug>[\w-]+)/(?P<slug>[\w-]+)/$',
        views.CourseDetailView.as_view(), name='details'),
    url(r'^(?P<category_slug>[\w-]+)/(?P<slug>[\w-]+)/comment/$',
        views.CommentFormView.as_view(), name='comment'),
)
