# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0003_auto_20141018_1133'),
    ]

    operations = [
        migrations.AddField(
            model_name='participation',
            name='creation_date',
            field=models.DateTimeField(default=datetime.date(2014, 10, 18), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='participation',
            unique_together=set([('course', 'user')]),
        ),
    ]
