# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('courses', '0002_auto_20141014_1911'),
    ]

    operations = [
        migrations.CreateModel(
            name='Participation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('is_owner', models.BooleanField(default=False)),
                ('role', models.CharField(max_length=20, default='student', choices=[('student', 'student'), ('teacher', 'teacher')])),
                ('status', models.CharField(max_length=20, default='pending', choices=[('pending', 'pending'), ('granted', 'granted'), ('denied', 'denied')])),
                ('course', models.ForeignKey(to='courses.Course')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='course',
            options={'ordering': ('-creation_date',)},
        ),
        migrations.RemoveField(
            model_name='course',
            name='author',
        ),
        migrations.AddField(
            model_name='course',
            name='participants',
            field=models.ManyToManyField(related_name='courses', to=settings.AUTH_USER_MODEL, through='courses.Participation'),
            preserve_default=True,
        ),
    ]
