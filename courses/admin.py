from django.contrib import admin

from .models import Course, Category


admin.site.register(Course, admin.ModelAdmin)
admin.site.register(Category, admin.ModelAdmin)
