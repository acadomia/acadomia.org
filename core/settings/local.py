from .base import *


DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['localhost']

INSTALLED_APPS += (
    'django_extensions',
    'debug_toolbar',
)

# JINGO_EXCLUDE_APPS += (
#     'debug_toolbar',
# )
