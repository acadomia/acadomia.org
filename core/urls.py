from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = patterns('',
    url(r'^cours/', include('courses.urls', namespace='courses')),

    url(r'^compte/', include('users.urls', namespace='users')),

    url(r'^admin/', include(admin.site.urls)),

    url(r'', include('default.urls')),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


# Jingo patch. See http://jingo.readthedocs.org/en/latest/#forms
import jingo.monkey
jingo.monkey.patch()
