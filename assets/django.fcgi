#!/home/scolia/venv/bin/python
import os, sys

_PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
_PROJECT_NAME = "scolia"

sys.path.insert(0, "/home/scolia/venv/lib/python3.4/site-packages")
sys.path.insert(0, _PROJECT_DIR)

os.environ['DJANGO_SETTINGS_MODULE'] = "core.settings.prod"

os.chdir(_PROJECT_DIR)

from django.core.servers.fastcgi import runfastcgi
runfastcgi(method="threaded", daemonize="false")
