from django.conf.urls import patterns, url

from . import views

urlpatterns = patterns('',
    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^tmp/(?P<filename>\w+)\.html$', views.TmpView.as_view()),
    url(r'^(?P<slug>[\w-]+)/$', views.GenericView.as_view(), name='page'),
    (r'^(?P<error_code>\d{3})/$', views.ErrorView.as_view()),
)
