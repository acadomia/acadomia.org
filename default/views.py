from django.views.generic import TemplateView
from django.template.loader import get_template
from django import template
from django.http import Http404


class TmpView(TemplateView):

    def get_template_names(self):
        return "tmp/%s.html" % self.kwargs['filename']


class HomeView(TemplateView):
    template_name = "default/home.html"


class GenericView(TemplateView):

    def get_template_names(self):
        template_name = "default/%s.html" % self.kwargs['slug']
        try:
            get_template(template_name)
            return template_name
        except template.TemplateDoesNotExist:
            raise Http404(
                "No template found matching name '%s'" % template_name)


class ErrorView(TemplateView):
    status_code = 500

    def get_template_names(self):
        return "/%s.html" % self.status_code
