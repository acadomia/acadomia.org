# -*- coding: utf-8 -*-
from django.views.generic import (CreateView as GenericCreateView,
                                  FormView, RedirectView, UpdateView,
                                  TemplateView)
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import get_user_model, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.messages.views import SuccessMessageMixin

from .models import Profile
from .forms import UserCreationForm, ProfileUpdateForm


User = get_user_model()


class DashboardView(TemplateView):
    template_name = "users/dashboard.html"


class UserCreateView(SuccessMessageMixin, GenericCreateView):
    model = User
    form_class = UserCreationForm
    template_name = "users/user_creation_form.html"
    success_url = reverse_lazy('users:login')
    success_message = ("Votre compte est créé, vous pouvez maintenant vous "
                       "enregistrer.")

    def form_valid(self, form):
        return super().form_valid(form)


class LoginView(SuccessMessageMixin, FormView):
    form_class = AuthenticationForm
    template_name = "users/login_form.html"
    success_url = reverse_lazy('users:dashboard')
    success_message = "Vous êtes maintenant authentifié."

    def form_valid(self, form, *args, **kwargs):
        login(self.request, form.get_user())
        return super().form_valid(form, *args, **kwargs)


class LogoutView(SuccessMessageMixin, RedirectView):
    url = "/"

    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)


class ProfileUpdateView(SuccessMessageMixin, UpdateView):
    model = Profile
    form_class = ProfileUpdateForm
    success_url = reverse_lazy('users:dashboard')
    success_message = "Votre profil a bien été mis à jour."

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def get_object(self, queryset=None):
        profile, c = Profile.objects.get_or_create(user=self.request.user)
        return profile
