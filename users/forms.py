from django import forms
from django.contrib.auth import get_user_model

from .models import Profile


User = get_user_model()


class UserForm(forms.ModelForm):
    class Meta:
        widgets = {
            'email': forms.widgets.EmailInput
        }


class UserCreationForm(UserForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email',)

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        exclude = ('user',)

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial', {})
        initial['user'] = kwargs.get('user')
        kwargs.update({'initial': initial})
        kwargs.pop('user')
        return super().__init__(*args, **kwargs)
